WITH
-- historical data filtered by country, platform, placement
hd as (
    select
            s.d, s.total_impressions, s.placement_id, s.country_id, s.platform_id
    from
            iow_rollups.iponweb_daily_stats s
    where
            -- go 30 days back
            s.d >= '2016-07-01' -- date_sub(NOW(), 30)
            -- country filter
            -- and s.country_id in (2)
            -- platform filter
            -- and s.platform_id in (1)
)
,
-- filter placements site or zone
plcmnt as (
    select
            distinct pl.placement_id, pl.publisher_id, pl.zone_id
    from
            metadata_db.placement pl
    where
            pl.publisher_id = 335
)


-- combine all of the filters together to get the dataset
/*select
        sub.d,
        sub.zone_id,
        sub.max_cnt as cnt
from (*/
select
        plcmnt.zone_id,
        hd.d,
        sum(hd.total_impressions) as cnt,
        max(sum(hd.total_impressions)) over (partition by d) max_cnt
from
        hd
        inner join plcmnt on hd.placement_id = plcmnt.placement_id
        inner join metadata_db.placement_size plsize on hd.placement_id = plsize.placement_id
group by
        plcmnt.zone_id, hd.d
order by
        hd.d, cnt desc
/*) sub
where
        sub.cnt = sub.max_cnt
order by
        sub.d*/
;