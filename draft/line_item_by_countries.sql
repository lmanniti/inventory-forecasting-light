-- filter by countries
select
        lic.line_item_id
from
        metadata_db.line_item_country lic
where
        lic.country_id in (?)
        and lic.exclude = 0
;