WITH
-- filter placements by id, site, zone
plcmnt as (
        select
                distinct pl.placement_id
        from
                metadata_db.placement pl
        where
                pl.placement_id in (230297,230773,523129)
                and pl.zone_id in (98119,98003,168951)
                and pl.site_id in (61457,527)
),
-- filter placements by platform
plplat as (
        select
                distinct pl.placement_id
        from
                metadata_db.placement pl
                inner join metadata_db.site s
                        on pl.site_id = s.site_id
                        and s.platform_id in (1)

),
-- filter placements by sizes
plsize as (
        select
                distinct pls.placement_id
        from
                metadata_db.placement_size pls
        where
                pls.size_id in (19,149,4)
),
-- combine the above filters
pl as (
        select
                plcmnt.placement_id
        from
                plcmnt
                inner join plsize on plcmnt.placement_id = plsize.placement_id
                inner join plplat on plcmnt.placement_id = plplat.placement_id
),
-- map placements to line items
lip as (
        select
                distinct lip.line_item_id
        from
                pl
                inner join metadata_db.line_item_placement lip
                        on pl.placement_id = lip.placement_id
                        and lip.exclude = 0
),
-- filter line items by countries
lic as (
        select
                distinct lic.line_item_id
        from
                metadata_db.line_item_country lic
        where
                lic.country_id in (195,2)
                and lic.exclude = 0
),
-- combine all of the filters together to get competing line items
li as (
        select
                li.*
        from
                metadata_db.line_item li
                inner join lip on li.line_item_id = lip.line_item_id
                inner join lic on li.line_item_id = lic.line_item_id
        where
                li.guaranteed = 1
                and li.status_id = 1
                -- extra check, guaranteed line items must normally have the start and end times specified
                and (li.start_time is not null and li.end_time is not null)
                -- that overlaps in time with current line item
                and (li.start_time < '2012-12-31' and li.end_time > '2012-11-11')
)
-- get the necessary information for competing line items
select
        li.line_item_id, li.impression_cap, li.impression_cap_is_daily, li.budget, li.budget_is_daily, li.start_time, li.end_time
from
        li
;