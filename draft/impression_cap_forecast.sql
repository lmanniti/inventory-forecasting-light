select 
        sub.id,
        sub.impression_cap * sub.days_overlap * 
                (CASE sub.impression_cap_is_daily WHEN 1 THEN 1 ELSE 1 / sub.duration END) 
        as 'impressions_overlap'
from (
        -- impression cap-based calculations for future line items
        select  
                li.id,              
                li.impression_cap,
                li.impression_cap_is_daily,
                datediff(li.end_time, li.start_time) + 1 as 'duration',
                datediff(
                        -- smallest end_time of the 2 line items
                        least(li.end_time, timestamp('2011-08-12 10:14:56')),
                        -- biggest start_time of the 2 line items
                        greatest(li.start_time, timestamp('2010-09-12 10:14:56'))
                ) + 1 as 'days_overlap'        
        from
                360yield.line_item li
        where
                li.guaranteed = 1
                -- active
                and li.status_id = 1
                -- extra check, guaranteed line items must normally have the start and end times specified
                and (li.start_time is not null and li.end_time is not null)
                -- that starts in the middle of the current line item
                and (li.start_time between timestamp('2010-09-12 10:14:56') and timestamp('2011-08-12 10:14:56'))
                -- calculations based on impression_cap only
                and (li.budget is null and li.impression_cap is not null)
) sub
;