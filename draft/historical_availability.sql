select
        sum(ds.total_impressions), ds.day
from (
        select
                s.day, s.total_impressions, s.placement_id
        from
                aggregation.iponweb_daily_stats_alex s

        where
                -- set how deep in the past we go
                s.day >= '2016-02-01'
                -- country filter (optional)
                and s.country_id = ?
                -- platform filter (optional)
                and s.platform_id = ?
                -- placement filter (optional)
                and s.placement_id = ?
) ds
inner join (
        select
                distinct placement_id
        from
                metadata_db.placement p
                inner join metadata_db.placement_size ps on
                        ps.placement_id = p.placement_id
                        -- publisher filter
                        and p.publisher_id = ?
                        -- site filter (optional)
                        and p.site_id = ?
                        -- zone filter (optional)
                        and p.zone_id = ?
        where
                -- sizes filter (optional)
                ps.size_id = ?
) ps
        on ds.placement_id = ps.placement_id
group by ds.day
order by ds.day;
