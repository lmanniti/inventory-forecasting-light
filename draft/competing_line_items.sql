SELECT
        li.id,
        li.start_time,
        li.end_time,
        CASE li.budget_is_daily
                WHEN 1 THEN li.budget * li.duration
                ELSE li.budget
        END as 'budget',
        CASE li.impression_cap_is_daily
                WHEN 1 THEN li.impression_cap * li.duration
                ELSE li.impression_cap
        END as 'impression_cap',
        lis.name
FROM (
        (
              SELECT
                      li.id,
                      li.start_time,
                      li.end_time,
                      li.budget,
                      li.budget_is_daily,
                      li.impression_cap,
                      li.impression_cap_is_daily,
                      datediff(li.end_time, li.start_time) + 1 as 'duration',
                      li.status_id
              FROM
                      360yield.line_item li
              WHERE
                      li.guaranteed = 1
                      -- active
                      and li.status_id = 1
        ) sub1
        inner join
        (
                SELECT
        )
                -- HERE NEEDS TO GO TARGETING CRITERIA FILTERING
                -- country    DP-1872
                -- platform   DP-1875
                -- placement  <-- placement
                -- site       <-- placement
                -- zone       <-- placement
                -- sizes      <-- placement_size

                -- dates      <-- line_item
) li
inner join 360yield.line_item_status_i18n lis
        on li.status_id = lis.id and lis.culture = 'en_GB'
;
