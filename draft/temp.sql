WITH
-- filter placements by id, site, zone
plcmnt as (        
        select
                distinct pl.placement_id, pl.zone_id, pl.site_id, s.platform_id
        from
                metadata_db.placement pl
                inner join metadata_db.site s
                        on pl.site_id = s.site_id
),
-- filter placements by sizes
plsize as (
        select
                distinct pls.placement_id, pls.size_id
        from
                metadata_db.placement_size pls
),
-- combine the above filters
pl as (
        select
                plcmnt.placement_id, plcmnt.zone_id, plcmnt.site_id, plcmnt.platform_id, plsize.size_id      
        from 
                plcmnt inner join plsize on plcmnt.placement_id = plsize.placement_id
),
-- map placements to line items
lip as (
        select
                distinct lip.line_item_id, pl.placement_id, pl.zone_id, pl.site_id, pl.size_id, pl.platform_id
        from
                pl
                inner join metadata_db.line_item_placement lip 
                        on pl.placement_id = lip.placement_id
                        and lip.exclude = 0
),
-- filter line items by countries
lic as (
        select
                distinct lic.line_item_id, lic.country_id
        from
                metadata_db.line_item_country lic
        where
                lic.exclude = 0
),
-- combine all of the filters together
li as (
        select
                lic.country_id, lip.*, li.start_time, li.end_time, li.publisher_id
        from 
                metadata_db.line_item li
                inner join lip on li.line_item_id = lip.line_item_id
                inner join lic on li.line_item_id = lic.line_item_id
        where
                li.guaranteed = 1
                and li.status_id = 1
                and li.start_time > '2012-04-04' and li.end_time < '2012-08-08'
),
-- 
lib as (
        select
                li.*, lib.cpm_bid
        from 
                li
                inner join metadata_db.line_item_bid lib on li.line_item_id = lib.line_item_id
        where
                lib.pricing_model_id = 1
)
select * from lib
;