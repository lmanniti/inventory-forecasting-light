select
        distinct ps.placement_id
from
        metadata_db.placement_size ps
where
        ps.size_id in (?)
;