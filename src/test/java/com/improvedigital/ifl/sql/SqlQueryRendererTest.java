package com.improvedigital.ifl.sql;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import freemarker.template.TemplateException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 *
 */
public class SqlQueryRendererTest {

	private static final String PUBLISHER_ID = "publisher_id";
	private static final String PLACEMENT_IDS = "placement_ids";
	private static final String ZONE_IDS = "zone_ids";
	private static final String SITE_IDS = "site_ids";
	private static final String SIZE_IDS = "size_ids";
	private static final String PLATFORM_IDS = "platform_ids";
	private static final String COUNTRY_IDS = "country_ids";
	private static final String START_TIME = "start_time";
	private static final String END_TIME = "end_time";
	private final SqlQueryRenderer victim = SqlQueryRenderer.getInstance();

	@Test
	public void renderCompetingLineItemsCompleteByZoneIds() throws Exception {
		Map<String, Object> params = ImmutableMap.<String, Object>builder()
				.put(PUBLISHER_ID, 189)
				.put(ZONE_IDS, Lists.newArrayList(98119, 98003, 168951))
				.put(SITE_IDS, Lists.newArrayList(61457, 527))
				.put(SIZE_IDS, Lists.newArrayList(4, 19, 149))
				.put(PLATFORM_IDS, Lists.newArrayList(1))
				.put(COUNTRY_IDS, Lists.newArrayList(2, 195))
				.put(START_TIME, "2012-11-11")
				.put(END_TIME, "2012-12-31")
				.build();

		assertTemplateRendered(params, COMPETING_LINE_ITEMS, "/competing_line_items_complete_zoneid.sql");
	}

	@Test
	public void renderCompetingLineItemsCompleteByPlacementIds() throws Exception {
		Map<String, Object> params = ImmutableMap.<String, Object>builder()
				.put(PUBLISHER_ID, 189)
				.put(PLACEMENT_IDS, Lists.newArrayList(230297, 230773, 523129))
				.put(ZONE_IDS, Lists.newArrayList(98119, 98003, 168951))
				.put(SITE_IDS, Lists.newArrayList(61457, 527))
				.put(SIZE_IDS, Lists.newArrayList(4, 19, 149))
				.put(PLATFORM_IDS, Lists.newArrayList(1))
				.put(COUNTRY_IDS, Lists.newArrayList(2, 195))
				.put(START_TIME, "2012-11-11")
				.put(END_TIME, "2012-12-31")
				.build();

		assertTemplateRendered(params, COMPETING_LINE_ITEMS, "/competing_line_items_complete_pid.sql");
	}

	@Test
	public void renderTotalBooked() throws Exception {
		Map<String, Object> params = ImmutableMap.<String, Object>builder()
				.put(PUBLISHER_ID, 64)
				.put(PLACEMENT_IDS, Lists.newArrayList(1288,1292,1309,1286,1295))
				.put(SIZE_IDS, Lists.newArrayList(13,18,221))
				.put(PLATFORM_IDS, Lists.newArrayList(1))
				.put(COUNTRY_IDS, Lists.newArrayList(2, 221))
				.put(START_TIME, "2012-06-20 00:00:00")
				.put(END_TIME, "2012-06-28 00:00:00")
				.build();

		assertTemplateRendered(params, TOTAL_BOOKED, "/total_booked_complete.sql");
	}

	@Test
	public void renderHistoricalCapacityByZoneId() throws Exception {
		Map<String, Object> params = ImmutableMap.<String, Object>builder()
				.put(PUBLISHER_ID, 35)
				.put(ZONE_IDS, Lists.newArrayList(582,98459,300413))
				.put(SIZE_IDS, Lists.newArrayList(4,171,637))
				.put(PLATFORM_IDS, Lists.newArrayList(1))
				.put(COUNTRY_IDS, Lists.newArrayList(16,57,9))
				.build();

		assertTemplateRendered(params, HISTORICAL_CAPACITY, "/historical_availability_complete_zoneid.sql");
	}

	@Test
	public void renderHistoricalCapacityByPlacementId() throws Exception {
		Map<String, Object> params = ImmutableMap.<String, Object>builder()
				.put(PUBLISHER_ID, 35)
				.put(PLACEMENT_IDS, Lists.newArrayList(5687,235545,835972))
				.put(ZONE_IDS, Lists.newArrayList(582,98459,300413))
				.put(SIZE_IDS, Lists.newArrayList(4,171,637))
				.put(PLATFORM_IDS, Lists.newArrayList(1))
				.put(COUNTRY_IDS, Lists.newArrayList(16,57,9))
				.build();

		assertTemplateRendered(params, HISTORICAL_CAPACITY, "/historical_availability_complete_pid.sql");
	}

	private static void assertTemplateRendered(Map<String, Object> params, Renderer renderer, String expected) throws
			IOException, TemplateException {
		assertEquals(IOUtils.toString(SqlQueryRendererTest.class.getResource(expected)), renderer.render(params));
	}

	private interface Renderer {
		String render(Map<String, Object> parameters) throws IOException, TemplateException;
	}

	private final Renderer TOTAL_BOOKED = new Renderer() {
		@Override
		public String render(Map<String, Object> parameters) throws IOException, TemplateException {
			return victim.renderTotalBookedSql(parameters);
		}
	};

	private final Renderer HISTORICAL_CAPACITY = new Renderer() {
		@Override
		public String render(Map<String, Object> parameters) throws IOException, TemplateException {
			return victim.renderHistoricalCapacitySql(parameters);
		}
	};

	private final Renderer COMPETING_LINE_ITEMS = new Renderer() {
		@Override
		public String render(Map<String, Object> parameters) throws IOException, TemplateException {
			return victim.renderCompetingLineItemsSql(parameters);
		}
	};

}