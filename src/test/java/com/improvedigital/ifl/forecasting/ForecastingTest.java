package com.improvedigital.ifl.forecasting;

import org.junit.Test;

import static com.improvedigital.ifl.forecasting.Forecasting.CYCLE_LENGTH;
import static com.improvedigital.ifl.forecasting.Forecasting.HISTORICAL_PERIOD;

/**
 *
 */
public class ForecastingTest {

	private final Forecasting openFixed = new OpenForecastingFixed();

	private final Forecasting openSelf = new OpenForecastingSelfTrained();

	private final Forecasting expo = new ExponentialSmooting();

	@Test
	public void forecastAvailableInventorySanomaNarrow() throws Exception {
		int[] data = new int[]{
				6667777, 3886833, 4328956, 7286400, 7488351, 7144958, 6640942, 7176789, 3868579, 4217867, 8043350,
				7531398, 7263050, 7135199, 8827478, 5338162, 4432955, 7289165, 7495350, 7717580, 6920042, 7324276,
				4112839, 3846394, 7341286, 7559969, 7133140, 6486380
		};

		forecast(expo, data);
		forecast(openFixed, data);
		forecast(openSelf, data);
	}

	@Test
	public void forecastAvailableInventorySanomaAll() throws Exception {
		int[] data = new int[]{
				48395180, 33212940, 37141254, 51314572, 53600523, 49433092, 42616612, 47817431, 33392624, 36951567,
				51845616, 48481984, 47670902, 45076094, 53068510, 40654660, 35268820, 44834357, 46049979, 49133948,
				44534081, 48299708, 33679853, 32807284, 47127004, 48862378, 47762178, 43041987
		};

		forecast(expo, data);
		forecast(openFixed, data);
		forecast(openSelf, data);
	}

	@Test
	public void forecastAvailableInventorySublimeNarrow() throws Exception {
		int[] data = new int[]{
				583578, 354369, 352833, 617882, 578003, 606357, 538343, 621686, 340189, 272356, 589916, 392017, 622551,
				434821, 506979, 419725, 426778, 626146, 642807, 613939, 675874, 680953, 462081, 480139, 739908, 741443,
				721592, 774587
		};

		forecast(expo, data);
		forecast(openFixed, data);
		forecast(openSelf, data);
	}

	@Test
	public void forecastAvailableInventorySublimeAll() throws Exception {
		int[] data = new int[]{
				6415340, 5827733, 5459904, 8781858, 8157410, 7525700, 5185832, 7274421, 4100980, 4402887, 9182874,
				9091119, 9234793, 7681822, 8674311, 6167879, 6407780, 10937617, 12236374, 11074669, 12074957, 12163896,
				8113576, 8586202, 11818119, 10852979, 9784498, 10622766
		};

		forecast(expo, data);
		forecast(openFixed, data);
		forecast(openSelf, data);
	}

	@Test
	public void forecastAvailableInventoryMarktplaatsAll() throws Exception {
		int[] data = new int[]{
				32334577, 31675950, 34794612, 31086572, 33499224, 29272196, 28882071, 29845565, 27655111, 29062442,
				37201938, 32333687, 31331411, 28405157, 25156412, 26497092, 28389533, 25486362, 23076631, 23273528,
				22594751, 23242544, 22899951, 25415765, 27547514, 25052728, 27572188, 24494154
		};

		forecast(expo, data);
		forecast(openFixed, data);
		forecast(openSelf, data);
	}

	private void forecast(Forecasting forecasting, int[] data) {
		System.out.println("Running forecasting with " + forecasting);
		int[] forecast = forecasting.runForecast(data, -HISTORICAL_PERIOD, CYCLE_LENGTH - 1);
		for (int i = -28; i < CYCLE_LENGTH; i++) {
			System.out.println(i + "\t" + forecast[i + HISTORICAL_PERIOD]);
		}
	}
}