<#include "/competing_line_items.ftl">,
-- get the necessary information for competing line items
libudget as (
        SELECT
                li.line_item_id,
                li.budget,
                li.budget_is_daily,
                li.impression_cap,
                li.impression_cap_is_daily,
                datediff(li.end_time, li.start_time) + 1 as 'duration',
                datediff(
                      -- smallest end_time of the 2 line items
                      least(li.end_time, '${end_time?date}'),
                      -- biggest start_time of the 2 line items
                      greatest(li.start_time, '${start_time?date}')
                ) + 1 as 'days_overlap',
                lib.cpm_bid
        FROM
                li
                inner join metadata_db.line_item_bid lib on
                      li.line_item_id = lib.line_item_id
                      -- take the last known cpm
                      and lib.end_time is null
                      -- only cpm-based line items (holds for guaranteed anyway)
                      and lib.pricing_model_id = 1
),
-- calculate impression_cap per line item
liimpressions as (
        select
                libudget.line_item_id,
                CASE
                        WHEN libudget.budget IS NOT NULL THEN libudget.budget / libudget.cpm_bid * 1000
                        ELSE libudget.impression_cap
                END as 'impression_cap',
                CASE
                        WHEN libudget.budget IS NOT NULL THEN libudget.budget_is_daily
                        ELSE libudget.impression_cap_is_daily
                END as 'is_daily',
                libudget.duration,
                libudget.days_overlap
        from
                libudget
),
-- calculate the amount of impressions that overlap with the current line item
lioverlap as (
        SELECT
                liimpressions.line_item_id,
                liimpressions.impression_cap * liimpressions.days_overlap * (CASE liimpressions.is_daily WHEN 1 THEN 1 ELSE 1 / liimpressions.duration END) as 'impressions_overlap'
        FROM
                liimpressions
)
select
        cast(sum(lioverlap.impressions_overlap) as int) as 'total_booked'
from
        lioverlap
;