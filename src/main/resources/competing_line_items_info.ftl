<#include "/competing_line_items.ftl">

-- get the necessary information for competing line items
select
        li.line_item_id, li.impression_cap, li.impression_cap_is_daily, li.budget, li.budget_is_daily, li.start_time, li.end_time
from
        li
;