<#setting number_format="computer">
<#setting date_format="yyyy-MM-dd">
<#function filterApplies values=[]>
    <#return (values?size > 1) || (values?size == 1 && values?first != -1)>
</#function>
<#function join values=[]>
    <#return values?join(",")>
</#function>
<#assign filterByPlacementIds = filterApplies(placement_ids)>
<#assign filterByZoneIds = filterApplies(zone_ids)>
<#assign filterBySiteIds = filterApplies(site_ids)>
<#assign filterByCountries = filterApplies(country_ids)>
<#assign filterBySizes = filterApplies(size_ids)>
<#assign filterByPlatforms = filterApplies(platform_ids)>