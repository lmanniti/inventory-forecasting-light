package com.improvedigital.ifl.forecasting;

import net.sourceforge.openforecast.DataSet;
import net.sourceforge.openforecast.models.TripleExponentialSmoothingModel;

/**
 *
 */
public class OpenForecastingSelfTrained extends OpenForecasting {

	@Override
	protected TripleExponentialSmoothingModel getModel(DataSet dataSet) {
		return TripleExponentialSmoothingModel.getBestFitModel(dataSet);
	}
}
