package com.improvedigital.ifl.forecasting;

/**
 *
 */
public interface Forecasting {

	// historical data is expected to be for last 28 days
	int HISTORICAL_PERIOD = 28;
	// seasonality of 1 week, e.g. 7 days
	int CYCLE_LENGTH = 7;
	// smoothing factor
	double ALPHA = 0.3d;
	// trend smoothing factor
	double BETA = 0.3d;
	// seasonal change smoothing factor
	double GAMMA = 0.3d;

	int[] runForecast(int[] historicalData, int start, int end);
}
