package com.improvedigital.ifl.forecasting;

import net.sourceforge.openforecast.DataSet;
import net.sourceforge.openforecast.Observation;
import net.sourceforge.openforecast.models.TripleExponentialSmoothingModel;

/**
 *
 */
public abstract class OpenForecasting extends AbstractForecasting<TripleExponentialSmoothingModel> {

	private static final String DAY = "day";

	@Override
	protected TripleExponentialSmoothingModel buildModel(int[] historicalData) {
		DataSet dataSet = new DataSet();
		for (int i = 0; i < historicalData.length; i++) {
			Observation observation = new Observation(historicalData[i]);
			observation.setIndependentValue(DAY, i - historicalData.length);
			dataSet.add(observation);
		}
		dataSet.setTimeVariable(DAY);
		dataSet.setPeriodsPerYear(CYCLE_LENGTH);
		return getModel(dataSet);
	}

	@Override
	protected int runForecast(TripleExponentialSmoothingModel model, int point) {
		Observation dataPoint = new Observation(0d);
		dataPoint.setIndependentValue(DAY, point);
		double forecast = model.forecast(dataPoint);
		return (int) forecast;
	}

	protected abstract TripleExponentialSmoothingModel getModel(DataSet dataSet);

}
