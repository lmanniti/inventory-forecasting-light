package com.improvedigital.ifl.forecasting;

import net.sourceforge.openforecast.DataSet;
import net.sourceforge.openforecast.models.TripleExponentialSmoothingModel;

/**
 *
 */
public class OpenForecastingFixed extends OpenForecasting {

	@Override
	protected TripleExponentialSmoothingModel getModel(DataSet dataSet) {
		TripleExponentialSmoothingModel model = new TripleExponentialSmoothingModel(ALPHA, BETA, GAMMA);
		model.init(dataSet);
		return model;
	}
}
