package com.improvedigital.ifl.forecasting;

import com.nc.tsa.HoltWinters;

/**
 *
 */
public class ExponentialSmooting extends AbstractForecasting<double[]> {

	@Override
	protected double[] buildModel(int[] historicalData) {
		long[] result = new long[historicalData.length];
		for (int i = 0; i < historicalData.length; i++) {
			result[i] = (long) historicalData[i];
		}
		return HoltWinters.forecast(result, ALPHA, BETA, GAMMA, CYCLE_LENGTH, CYCLE_LENGTH);
	}

	@Override
	protected int runForecast(double[] model, int point) {
		return (int) model[point + 28];
	}
}
