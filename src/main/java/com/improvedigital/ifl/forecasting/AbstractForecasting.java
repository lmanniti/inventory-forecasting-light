package com.improvedigital.ifl.forecasting;

/**
 *
 */
public abstract class AbstractForecasting<T> implements Forecasting {

	@Override
	public int[] runForecast(int[] historicalData, int start, int end) {
		if (historicalData == null || historicalData.length != HISTORICAL_PERIOD) {
			throw new IllegalArgumentException("Wrong historical data size");
		}

		T model = buildModel(historicalData);

		int[] result = new int[end - start + 1];
		for (int i = start; i <= end; i++) {
			result[i - start] = runForecast(model, i);
		}
		return result;
	}

	protected abstract T buildModel(int[] historicalData);

	protected abstract int runForecast(T model, int point);
}
