package com.improvedigital.ifl.step;

import com.improvedigital.ifl.sql.SqlQueryRenderer;
import freemarker.template.TemplateException;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.exception.KettleValueException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.trans.steps.userdefinedjavaclass.FieldHelper;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClass;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassData;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassMeta;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
abstract class AbstractRenderSqlQueryStep extends AbstractKettleToJavaAdapter<Object[]> {

	AbstractRenderSqlQueryStep(UserDefinedJavaClass parent,
			UserDefinedJavaClassMeta meta,
			UserDefinedJavaClassData data) throws KettleStepException {
		super(parent, meta, data, 1);
	}

	@Override
	protected Object[] transformInputRows(RowMetaInterface inputMetadata, List<Object[]> inputRows) {
		// get the first and the only row
		return inputRows.get(0);
	}

	@Override
	protected Object processInput(Object[] input) throws KettleException, IOException, TemplateException {
		return renderSql(SqlQueryRenderer.getInstance(), getParameters(input));
	}

	protected abstract String renderSql(SqlQueryRenderer renderer, Map<String, Object> parameters) throws IOException,
			TemplateException;

	private Map<String, Object> getParameters(Object[] input) throws KettleValueException, KettleStepException {
		try {
			Map<String, Object> parameters = new HashMap<>();
			for (ValueMetaInterface valueMeta : data.inputRowMeta.getValueMetaList()) {
					parameters.put(valueMeta.getName(), getValue(input, valueMeta));
			}
			return parameters;
		} catch (ParseException e) {
			throw new KettleValueException(e);
		}
	}

	private Object getValue(Object[] input, ValueMetaInterface valueMeta) throws KettleValueException,
			KettleStepException, ParseException {
		FieldHelper inputField = get(Fields.In, valueMeta.getName());
		switch (valueMeta.getType()) {
			case ValueMetaInterface.TYPE_STRING:
				return toIntegers(inputField.getString(input));
			case ValueMetaInterface.TYPE_INTEGER:
				return inputField.getInteger(input);
			case ValueMetaInterface.TYPE_DATE:
				return new SimpleDateFormat("yyyy-MM-dd").parse(inputField.getString(input));
			default:
				throw new UnsupportedOperationException("Unsupported value type: " + valueMeta);
		}
	}

	private int[] toIntegers(String value) throws KettleValueException, KettleStepException {
		String input = value.trim();
		if (input.isEmpty()) {
			return new int[0];
		}

		String[] source = input.split(",");
		int[] result = new int[source.length];

		for (int i = 0; i < source.length; i++) {
			result[i] = Integer.valueOf(source[i].trim());
		}

		return result;
	}
}
