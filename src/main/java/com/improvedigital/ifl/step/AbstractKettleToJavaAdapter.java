package com.improvedigital.ifl.step;

import com.improvedigital.ifl.util.Util;
import freemarker.template.TemplateException;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.exception.KettleValueException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.userdefinedjavaclass.TransformClassBase;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClass;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassData;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassMeta;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
abstract class AbstractKettleToJavaAdapter<D> extends TransformClassBase {

	private final int inputRowsSizeExpected;

	AbstractKettleToJavaAdapter(UserDefinedJavaClass parent,
			UserDefinedJavaClassMeta meta,
			UserDefinedJavaClassData data, int inputRowsSizeExpected) throws KettleStepException {
		super(parent, meta, data);
		this.inputRowsSizeExpected = inputRowsSizeExpected;
	}

	@Override
	public boolean processRow(StepMetaInterface smi, StepDataInterface sdi) throws KettleException {
		String[] outputFieldNames = data.outputRowMeta.getFieldNames();
		Util.checkArgument(outputFieldNames.length == 1, "Expected only 1 output field");

		List<Object[]> inputRows = new ArrayList<>();
		Object[] inputRow;
		while ((inputRow = getRow()) != null) {
			inputRows.add(inputRow);
		}

		if (inputRowsSizeExpected >= 0) {
			int inputRowsSize = inputRows.size();
			String message = "Expected " + inputRowsSizeExpected + " rows, got " + inputRowsSize;
			Util.checkArgument(inputRowsSize == inputRowsSizeExpected, message);
		}

		D inputData = transformInputRows(data.inputRowMeta, inputRows);
		Object output;
		try {
			output = processInput(inputData);
		} catch (TemplateException | IOException | RuntimeException e) {
			throw new KettleException(e);
		}

		Object[] outputRow = createOutputRow(null, data.outputRowMeta.size());
		get(Fields.Out, outputFieldNames[0]).setValue(outputRow, output);

		putRow(data.outputRowMeta, outputRow);

		setOutputDone();
		return false;
	}

	protected abstract D transformInputRows(RowMetaInterface inputMetadata, List<Object[]> inputRows) throws
			KettleStepException, KettleValueException;

	protected abstract Object processInput(D input) throws KettleException, IOException, TemplateException;
}
