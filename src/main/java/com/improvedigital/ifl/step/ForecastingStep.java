package com.improvedigital.ifl.step;

import com.improvedigital.ifl.forecasting.Forecasting;
import com.improvedigital.ifl.forecasting.OpenForecastingSelfTrained;
import com.improvedigital.ifl.util.Util;
import freemarker.template.TemplateException;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.exception.KettleValueException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.steps.userdefinedjavaclass.FieldHelper;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClass;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassData;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassMeta;

import java.io.IOException;
import java.util.List;

/**
 *
 */
public class ForecastingStep extends AbstractKettleToJavaAdapter<int[]> {

	private final Forecasting forecasting = new OpenForecastingSelfTrained();

	public ForecastingStep(UserDefinedJavaClass parent,
			UserDefinedJavaClassMeta meta,
			UserDefinedJavaClassData data) throws KettleStepException {
		super(parent, meta, data, -1);
	}

	@Override
	protected int[] transformInputRows(RowMetaInterface inputMetadata, List<Object[]> inputRows) throws
			KettleStepException, KettleValueException {
		String[] fieldNames = inputMetadata.getFieldNames();
		Util.checkArgument(fieldNames.length == 1, "Expected 1 input field, but got " + fieldNames.length);
		FieldHelper fieldHelper = get(Fields.In, fieldNames[0]);

		int[] result = new int[inputRows.size()];
		int i = 0;
		for (Object[] row : inputRows) {
			result[i++] = fieldHelper.getInteger(row).intValue();
		}
		return result;
	}

	@Override
	protected Long processInput(int[] input) throws KettleException, IOException, TemplateException {
		int[] forecast = forecasting.runForecast(input, 0, Forecasting.CYCLE_LENGTH - 1);

		long sum = 0;
		for (int datapoint : forecast) {
			sum += datapoint;
		}
		return sum;
	}

}
