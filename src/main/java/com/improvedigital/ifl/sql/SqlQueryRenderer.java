package com.improvedigital.ifl.sql;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

/**
 *
 */
public class SqlQueryRenderer {

	private static final SqlQueryRenderer INSTANCE = new SqlQueryRenderer();

	public static SqlQueryRenderer getInstance() {
		return INSTANCE;
	}

	private final Configuration cfg;

	private SqlQueryRenderer() {
		// Create your Configuration instance, and specify if up to what FreeMarker
		// version (here 2.3.25) do you want to apply the fixes that are not 100%
		// backward-compatible. See the Configuration JavaDoc for details.
		cfg = new Configuration(Configuration.VERSION_2_3_25);

		// Specify the source where the template files come from. Here I set a
		// plain directory for it, but non-file-system sources are possible too:
		cfg.setClassLoaderForTemplateLoading(getClass().getClassLoader(), "/");

		// Set the preferred charset template files are stored in. UTF-8 is
		// a good choice in most applications:
		cfg.setDefaultEncoding("UTF-8");

		// Sets how errors will appear.
		// During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

		// Don't log exceptions inside FreeMarker that it will thrown at you anyway:
		cfg.setLogTemplateExceptions(false);
	}

	public String renderCompetingLineItemsSql(Map<String, Object> parameters) throws IOException, TemplateException {
		return renderTemplate(parameters, "competing_line_items_info.ftl");
	}

	public String renderTotalBookedSql(Map<String, Object> parameters) throws IOException, TemplateException {
		return renderTemplate(parameters, "total_booked.ftl");
	}

	public String renderHistoricalCapacitySql(Map<String, Object> parameters) throws IOException, TemplateException {
		return renderTemplate(parameters, "historical_capacity.ftl");
	}

	private String renderTemplate(Map<String, Object> parameters, String templateName) throws IOException,
			TemplateException {
		Template template = cfg.getTemplate(templateName);
		StringWriter out = new StringWriter();
		template.process(parameters, out);
		return out.toString();
	}
}
